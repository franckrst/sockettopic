import { Topic } from "./Topic/Topic";
import { ConnectionClient } from "./ConnectionClient";
import * as Http from "http";
import { request, server as WebSocketServer } from "websocket";
import { Server as HttpServer } from "http";

export class Server {

  public static topics: Topic[] = [];

  public static connectionsClients: ConnectionClient[] = [];

  private static httpServer: HttpServer = Http.createServer(request => {});

  private static webSocketServer: WebSocketServer = new WebSocketServer({ httpServer: Server.httpServer});

  public static start(): void {
    Server.httpServer.listen(1664);
    Server.webSocketServer.on('request', Server.request.bind(Server));
  }
  public static getTopic(name: string, silence?: boolean): Topic{
    let topic: Topic = Server.topics.find((topic: Topic) => topic.name === name );
    if (topic) {
      return topic;
    } else {
      topic = new Topic(name, silence);
      this.topics.push(topic);
      return topic;
    }
  }

  public static getServerTopic(): Topic {
    return this.getTopic('__Server', true);
  }

  private static request(request: request): void {

    Server.connectionsClients.push(new ConnectionClient(request));
  }

}