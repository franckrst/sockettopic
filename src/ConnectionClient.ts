import {connection, IMessage, request} from "websocket";
import {Server} from "./Server";
import {TopicMessage, TopicMessageType} from "./Topic/ClientMessage";
import {ServerMessage, ServerMessageType} from "./Topic/ServerMessage";

export class ConnectionClient {

  public socket: connection;
  public name: string;

  constructor(request: request){
    this.name = request.httpRequest.url.replace('/', '');
    this.socket = request.accept(null, null);
    this.socket.on('message', this.onMessage.bind(this));
    this.socket.on('close', this.onClose.bind(this));
    Server.getServerTopic().addClient(this);
  }

  public send(message: ServerMessage): void{
    console.log('send: ', message);
    this.socket.sendUTF(JSON.stringify(message.serialize()));
  }

  /**
   * - subscription
   * Subscribe('channel')
   * unSubscribe('channel')
   * - custome
   * data
   * @param message
   */
  private onMessage(message: IMessage): void {
    console.log('recieve: ', message);
    let topicMessage: TopicMessage;
    try {
      topicMessage = new TopicMessage(this, JSON.parse(message.utf8Data));
    } catch (e){
      this.send(new ServerMessage(ServerMessageType.ERROR, Server.getServerTopic(), e.toString()));
      return;
    }
    switch (topicMessage.type) {
      case TopicMessageType.SUBSCRIBE:
        topicMessage.topic.addClient(this);
        break;
      case TopicMessageType.UNSUBSCRIBE:
        topicMessage.topic.removeClient(this);
        break;
      case TopicMessageType.INFO:
        topicMessage.topic.info(this);
        break;
      case TopicMessageType.ACTION:
        topicMessage.topic.sendAll(ServerMessageType.ACTION, topicMessage.data);
        break;
      default:

    }
  }

  private onClose(code: number, desc: string): void {
    Server.topics.forEach((topic) => { topic.removeClient(this) });
  }

}
