import {ConnectionClient} from "../ConnectionClient";
import {ServerMessage, ServerMessageType} from "./ServerMessage";

export class Topic {

  private connectionsClients: ConnectionClient[] = [];

  constructor(public name: string, private silence: boolean = false){}

  public addClient(connectionClient: ConnectionClient): void{
    this.info(connectionClient);
    if(this.connectionsClients.indexOf(connectionClient) === -1){
      this.sendAll(ServerMessageType.SUBSCRIBE,  connectionClient.name);
      this.connectionsClients.push(connectionClient);
    }
  }

  public info(connectionClient: ConnectionClient): void {
    this.sendTo(connectionClient,ServerMessageType.TOPICSTATUS,this.connectionsClients.map((client)=>client.name));
  }

  public removeClient(connectionClient: ConnectionClient): void {
    if(this.connectionsClients.indexOf(connectionClient) == -1){ return; }
    this.connectionsClients.splice(this.connectionsClients.indexOf(connectionClient), 1);
    this.sendAll(ServerMessageType.UNSUBSCRIBE, connectionClient.name);
  }

  public sendTo(connectionClient: ConnectionClient, type: ServerMessageType, data: any){
    if(this.silence) { return; }
    connectionClient.send(new ServerMessage(type, this, data));
  }

  public sendAll(type: ServerMessageType, data:any): void {
    if(this.silence) { return; }
    this.connectionsClients.forEach((connectionClient: ConnectionClient) => {
      this.sendTo(connectionClient, type, data);
    });
  }

}