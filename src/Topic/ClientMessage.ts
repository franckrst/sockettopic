import {ConnectionClient} from "../ConnectionClient";
import {Topic} from "./Topic";
import {Server} from "../Server";

export enum TopicMessageType {
    SUBSCRIBE   = 'subscribe',
    UNSUBSCRIBE = 'unsubscribe',
    ACTION      = 'action',
    INFO        = 'info'
}
export class TopicMessage {

    public type: TopicMessageType;
    public from: ConnectionClient;
    public data : any;
    public topic: Topic;

    constructor(from: ConnectionClient, data: {type:TopicMessageType, topic: string, data:any}) {
        this.type = data.type;
        this.from = from;
        this.data = data.data;
        this.topic = Server.getTopic(data.topic);
    }

    public serialize(){
        return {
            from : this.from.name,
            type: this.type,
            data: this.data
        };
    }
}