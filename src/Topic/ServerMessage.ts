import {ConnectionClient} from "../ConnectionClient";
import {Topic} from "./Topic";
import {Server} from "../Server";

export enum ServerMessageType {
  TOPICSTATUS = 'topicStatus',
  SUBSCRIBE = 'subscribe',
  UNSUBSCRIBE = 'unsubscribe',
  ACTION = 'action',
  ERROR = 'error'
}

/**
 * Server
 */
export class ServerMessage {

  constructor(
    public type:ServerMessageType,
    public topic: Topic,
    public data:any) {
  }

  public serialize(){
    return {
      type: this.type,
      topic: this.topic.name,
      data: this.data
    };
  }
}