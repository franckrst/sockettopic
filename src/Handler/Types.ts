
export type Middleware<T, T1> = (req: T, res: End<T1>, next: () => void) => void;

export type End<T> = (dataOutput: T) => void;
export type Call<T,T1> = (dataInput: T, res: (dataOutput: T1) => void) => void;