import { Middlewares } from './Middlewares';
import { Call, End } from './Types';

/**
 * @T : Input
 * @T1 : Output
 */
export class Handler<T, T1> {

  constructor(
    public middlewares: Middlewares<T, T1>
  ){ }

  public start(inputData: T, call: Call<T, T1>, end: End<T1>): void {
      this.runBefor(0, inputData, call, end);
  }

  public runBefor(index: number, inputData: T, res: Call<T, T1>, end: End<T1>): void {
    if (this.middlewares.befor.length > index) {
      this.middlewares.befor[index](
        inputData,
        (outputData: T1) => this.runAfter(0, outputData, end),
        () => this.runBefor(index+1, inputData, res, end)
      );
    } else {
      res(inputData,(outputData: T1) => this.runAfter(0, outputData, end));
    }
  }

  public runAfter(index: number, outputData:T1, end: End<T1>): void {
    if (this.middlewares.after.length > index) {
      this.middlewares.after[index](
          outputData,
          end,
          () => this.runAfter(index+1, outputData,  end)
          );
    }else{
      end(outputData);
    }
  }

}