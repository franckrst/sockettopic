import {Handler} from "./Handler";
import {Middlewares} from "./Middlewares";

export function todo() {
    let middlewares = new Middlewares<{ client: any }, string>([], []);

    middlewares.befor.push((req, res, next) => {
        console.log("=B1=>", req);
        req.client = req.client + "s";
        next();
    });

    middlewares.befor.push((req, res, next) => {
        console.log("=B2=>", req);
        req.client = req.client + "s";
        next();
    });

    middlewares.after.push((res, end, next) => {
        console.log("=A1=>", res);
        console.log("=A=>", res);
        res = res + "s";
        next();
    });
    middlewares.after.push((res, end, next) => {
        console.log("=A2=>", res);
        console.log("=A=>", res);
        res = res + "s";
        next();
    });

    new Handler(middlewares).start({client: "machin"}, (req, res) => {
        res(req.client);
    }, (dataOutput) => {
        console.log(dataOutput);
    });
}