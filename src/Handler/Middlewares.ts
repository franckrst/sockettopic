import { Middleware } from './Types';

export class Middlewares<T, T1> {
  constructor(
    public befor: Middleware<T,T1>[] = [],
    public after: Middleware<T1,T1>[] = []
  ) { }
}


